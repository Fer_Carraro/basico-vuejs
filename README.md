# Aprendiendo Vue JS #
Vue.js es un framework similar a Angular & React JS.

Hay dos formas para trabajar con Vue JS.

1. Incluir un archivo JS en la página HTML.
```html
<script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
```

2. Instalando Vue Cli con NPM.
```bash
$ npm install -g @vue/cli
$ npm list
$ vue --version
```

## Crear una aplicación Vue JS 
```bash
$ vue create proyecto
$ cd proyecto && tree
$ npm run serve
```

Abrir el navegador en http://localhost:8080


Enlaces:
[Sitio oficial](https://vuejs.org)
[Axios](https://es.vuejs.org/v2/cookbook/using-axios-to-consume-apis.html)
[Vue School](https://vueschool.io/lessons/install-vue-cli-and-its-dependencies)
[Javascript Info](https://javascript.info/)
[Vue Mastery](https://www.vuemastery.com/courses/vue3-deep-dive-with-evan-you/)